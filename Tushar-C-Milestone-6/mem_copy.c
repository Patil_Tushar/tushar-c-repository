void *mem_copy(void *olddest, const void *oldsrc, unsigned int n) {
    assert((src != NULL) && (n > 0));  
    int i = 0;
    char *newsrc = (char*)oldsrc;
    char *newdest = (char*)olddest;
    while (i < n) {
        newdest[i] = newsrc[i];
        i++;
    }
    newdest[i]='\0';
    return newdest;
} 
